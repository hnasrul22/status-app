/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    colors : {
      white : '#F7F1E5',
      gold : '#E7B10A',
      'light-green' : '#898121',
      green : '#4C4B16',
      trans : 'transparent',
      red : '#df2c14'
    }
  },
  plugins: [],
}
