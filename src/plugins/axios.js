import axios from "axios"

/**
 * @param {{baseURL}} options 
 * @returns AxiosInstance
 */
export const setupAxios = (options) => {
    return axios.create({
        baseURL : options.baseURL
    })
}

export default {
    install : (app,options) => {
        app.config.globalProperties.$axios = setupAxios(options)
    }
}