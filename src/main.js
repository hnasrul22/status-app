import { createApp } from 'vue'
import App from './App.vue'
import Box from './components/Box.vue'
import axios from './plugins/axios'
import store from './store'

import './assets/main.css'

createApp(App)
.use(axios,{
    baseURL : import.meta.env.VITE_BASE_URL
})
.use(store)
.component('box',Box)
.mount('#app')
