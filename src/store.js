import { createStore } from "vuex";
import { setupAxios } from "./plugins/axios";

const axios = setupAxios({
    baseURL : import.meta.env.VITE_BASE_URL
})

const store = createStore({
    state(){
        return {
            allTweets : [],
            loading : false
        }
    },
    mutations : {
        updateAllTweets(state,payload){
            state.allTweets = payload
        },
        setLoading(state,payload){
            state.loading = payload
        }
    },
    actions : {
        async synchronize({commit}){
            try{
                const {data} = await axios.get('/tweets',{
                    params : {
                        _sort : 'created',
                        _order : 'desc',
                        _embed : 'comments'
                    }
                })
                commit('updateAllTweets',data)
            }catch(e){
                console.error(e);
            }
        },
        
        getAllTweets({commit,dispatch}){
            commit('setLoading',true)
            dispatch('synchronize')
            commit('setLoading',false)
        },

        async deleteComment({dispatch},id){
            try{
                await axios.delete(`/comments/${id}`)
                dispatch('synchronize')
            }catch(e){
                console.error(e)
            }
        },

        async deleteTweet({dispatch},id){
            try{
                await axios.delete(`/tweets/${id}`)
                dispatch('synchronize')
            }catch(e){
                console.error(e)
            }
        },

        async addTweet({dispatch},newTweet){
            try{
                const user = JSON.parse(localStorage.getItem('user'))
    
                await axios.post('/tweets',{
                        user,
                        text : newTweet,
                        created : new Date().getTime(),
                        like : 0,
                        retweet : 0,
                    })
                
                dispatch('synchronize')

            }catch(e){
                console.error(e.response);
            }
        },

        async addComment({dispatch},{tweetId,newComment}){
            try{
                const user = JSON.parse(localStorage.getItem('user'))
    
                await axios.post('/comments',{
                        user,
                        text : newComment,
                        tweetId,
                        created : new Date().getTime(),
                        like : 0,
                        retweet : 0,
                    })

                dispatch('synchronize')

            }catch(e){
                console.error(e);
            }
        }
    }
})

export default store